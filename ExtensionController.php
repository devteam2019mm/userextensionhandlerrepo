<?php

class ExtensionController extends BaseController {

	//function to update an extension int the database, we also call a ext an user
    public function updateUser()
    {
        $data = Input::all(); //geting all the data sent 
        $json = $data['json'];//getting the json object sent
        if (!isset($json) || null == $json) 
            return "0"; //return if there is no json or if it is null

                $jsonUser = json_decode($json);

        if (!isset($jsonUser->UserIDNo) || null == $jsonUser->UserIDNo) //return if there is no userId
            return "0";

        try
        {
            //find the ext using the id and update it 
			$user = User::find($jsonUser->UserIDNo);
            $this->json_to_user($jsonUser, $user);
            $user->save();
            $this->refreshUsers();
            return "1";
        } catch (Exception $e) {
            return "0";
        }
    }

	public function insertUser()
	{
        $data = Input::all();
        $json = $data['json'];

        if (!isset($json) || null == $json)
            return "0";

        try
        {
            $jsonUser = json_decode($json);
            $user = new User;
            $this->json_to_user($jsonUser, $user);
			//insert the ext if doesn't exist
            if (User::find($user->UserIDNo) != null)
            {
                return "User already exists";
            }

            $user->save();
            $this->refreshUsers();
            return "1";
        } catch (Exception $e) {
            return $e->getMessage();
        }
	}

    public function deleteUsers()
	{
        $data = Input::all();

        if (isset($data['unknown']))
        {
            //deleting all Unknown extensions
			User::where('Name','like',"(Unknown)")->delete();
            $this->refreshUsers();
            return '1';
        }

        if (isset($data['json']))
        {
            $json = $data['json'];
        }
        else
        {
            return '0';
        }

        try
        {
            $jsonArray = json_decode($json, true);
            if (count($jsonArray) == 0)
                return 'empty';
            //deleting the ext
            User::destroy($jsonArray);
            $this->refreshUsers();
            return '1';
        } catch (Exception $e) {
            return '0';
        }
	}

    private function deleteAllUsers()
    {
        User::truncate();
        $this->refreshUsers();
    }

	function to export the extensions to a text file
    public function export($type = 'csv')
    {
        if($type != 'csv' && $type != 'txt')
            return;

        $pathToFile = Config::get('app.MyStorage').'exts.'.$type;

        $DivAName = Ini0::find("DivisionLevelADescS")->Value;
        $DivBName = Ini0::find("DivisionLevelBDescS")->Value;
        $DivCName = Ini0::find("DivisionLevelCDescS")->Value;
        $DivDName = Ini0::find("DivisionLevelDDescS")->Value;
        $DivEName = Ini0::find("DivisionLevelEDescS")->Value;

        $headerArray = array("UserID","Ext","Name",$DivAName,$DivBName,$DivCName,$DivDName,$DivEName,
                                "Site#","Site","Account","Rate","Color","Notes");

        $firstLine ='';
        foreach($headerArray as $header)
        {
            if ($type == 'csv')
                $header = '"'.$header.'"';
            $firstLine .= $header.",";
        }

        $firstLine = rtrim($firstLine, ",")."\r\n";
        file_put_contents($pathToFile, $firstLine);

        $paramArray = array("UserID",
                            "Ext",
                            "Name",
                            "DivA",
                            "DivB",
                            "DivC",
                            "DivD",
                            "DivE",
                            "SiteNo",
                            "Site",
                            "AuthCode",
                            "RateNo",
                            "Color",
                            "Notes",);

        $extensions = User::all();

        $divA = DivA::all();
        $divB = DivB::all();
        $divC = DivC::all();
        $divD = DivD::all();
        $divE = DivE::all();
        $sites = Site::select('SiteNo','SiteLongName')->get();

        foreach($extensions as $ext)
        {
            $str ='';

            foreach(Helper::$types as $t)
            {
                if ( isset($ext->{'Div'.$t.'No'}) && isset( ${'div'.$t} ) && !${'div'.$t}->isEmpty() )
                {
                    $div = ${'div'.$t}->find($ext->{'Div'.$t.'No'});
                    if (isset($div) && isset($div->{'Div'.$t.'Name'}) )
                    {
                        $ext->{'Div'.$t} = $div->{'Div'.$t.'Name'};
                    }
                }
            }
     
            $ext->Site = $sites->isEmpty() ? 'Default Site' : $sites->find($ext->SiteNo)->SiteLongName;

            foreach($paramArray as $param)
            {
                $val = $ext->$param;
                if ($type == 'csv')
                    $val = '"'.$val.'"';

                $str .= $val.",";
            }
            $str = rtrim($str, ",")."\r\n";
            file_put_contents($pathToFile, $str, FILE_APPEND);

        }
        $headers = array();
        if ($type == 'csv')
        {
            $headers =  array('Content-type' => 'text/csv', 'Content-disposition' => 'attachment');
        }

        return Response::download($pathToFile, 'exts.'.$type, $headers);
    }

    //Warning: $extension->DivTNo is the name until set mid way through function
    public function import()
    {
        $sites = Site::select('SiteNo','SiteLongName')->get();

        $columns = array(
            'UserID',
            'Ext',
            'Name',
            'Email',
            'AuthCode',
            'Notes',
            'LoginA',
            'LoginB',
            'SiteNo',
            'DivANo',
            'DivBNo',
            'DivCNo',
            'DivDNo',
            'DivENo',
            'RateNo',
            'Color',
            'Print',
            'Device',
            'Private',
            'PMSPost',
            'Auto',
            'UserIDNo',
            'ExtNo',
            'SENo',
        );

        $addExt = Input::get('addExt', true); //append or overwrite
        $skip = Input::get('skip', false); //append or overwrite
        $format = Input::all();

        if (Input::hasFile('file') === false)
        {
            echo ' no file ';
            return;
        }

        $file = Input::file('file');
     
        $fileType = $file->getClientOriginalExtension();
        if ($fileType != 'csv') //remove extra quotations
        {
            return "Invalid file type, only .csv format accepted";
        }


        if (($handle = fopen($file->getRealPath(), "r")) === false) {
            return;
        }

        $extensions = array();
        $extNums = array();
        $i = 0;

        $divsANos = array( 'Default' => 1);
        $divsBNos = array( );
        $divsCNos = array( );
        $divsDNos = array( );
        $divsENos = array( );

        while (($line = fgetcsv($handle)) !== FALSE) {
            if ($skip)
            {//skip the first line
                $skip = false;
                continue;
            }

            $extension = array();
            //get each column from the file and assign it to extension
            foreach($columns as $col)
            {
                if (isset($format[$col])) //if zero, column is not in file
                {
                    if ($format[$col] != 0)
                    {
                        $extension[$col] = trim($line[$format[$col]-1]); //1 based to 0 based
                    }

                    //if is a divs column and column number is set to zero or extension->col equals an empty string
                    // set to 'Default'
                    if (strpos($col, 'Div') !== false && ($format[$col] == 0 || $extension[$col] == ""))
                    {
                        $extension[$col] = 'Default';
                    }
                }
            }

            if ( $extension["UserID"] == "" && $extension["Ext"] == "")
            {//I award you no points, and may god have mercy on your soul.
                continue;
            }
            else if ($extension["UserID"] == "" && $extension["Ext"] != "")
            {
                $extension["UserID"] = $extension["Ext"];
            }
            else if ($extension["Ext"] == "" && $extension["UserID"] != "")
            {
                $extension["Ext"] = $extension["UserID"];
            }

            if( !is_numeric($extension["Ext"]) )
            {
                //dd($extension["Ext"]);
                continue;
            }

            $extension["ExtNo"] = $extension["Ext"];
            $extension["UserIDNo"] = is_numeric($extension["UserID"]) ? $extension["UserID"] : $extension["ExtNo"];

            //find keys based on name values, 5 table lookups

            if (isset($extension["SiteNo"])) 
            {
                $siteNo = $this->findPrim($sites, 'SiteLongName', $extension["SiteNo"]);
            }
            $extension["SiteNo"] = isset($siteNo) ? $siteNo : 0;

            //slow because of qeueries required, I should save the information I'm looking up as I go.
            //add unfound divs
            foreach(Helper::$types as $t)
            {
                if (!isset($extension['Div'.$t.'No']))
                {
                    //$extension['Div'.$t.'No'] = 1;
                    continue;
                }
                $Div = 'Div'.$t;
                $divsNos = 'divs'.$t.'Nos';
                $divName = $extension['Div'.$t.'No'];

                //if we've seen this (name => #) before, use it!
                if ( isset( ${$divsNos}[ $extension['Div'.$t.'No'] ] ) )
                {
                    $extension['Div'.$t.'No'] = ${$divsNos}[$divName];
                }
                else
                {// look it up!
                    $div= $Div::where('Div'.$t.'Name', "=", $divName)->first();
                    if ( !isset($div))
                    { //Doesn't exist? Add it!
                        $divController = new AssignDivsController();
                        $div = new $Div;
                        $div->{'Div'.$t.'Name'} = $divName;
                        $extension['Div'.$t.'No'] = $divController->insertDivWithDiv($div, $t);
                    }
                    else
                    { //Found it!
                        $extension['Div'.$t.'No'] = $div->{'Div'.$t.'No'};
                    }

                    //add to divsNos, save it for next time!
                    ${$divsNos}[$divName] = $extension['Div'.$t.'No'];
                }
            }
      
            if (!isset($extension["Color"]))
                $extension["Color"] = 0; //default black

            //make sure the imported extensions are unique
            // by taking advantage of PHP's array implementation
            if (!isset($flippedExtNums[$extension["ExtNo"]]))
            {//is unique
                $extensions[] = $extension;
                $flippedExtNums[$extension["ExtNo"]] = $i;
                $extNums[] = $extension["ExtNo"];
            }
            else
            {
                $extensions[$flippedExtNums[$extension["ExtNo"]]] = $extension;
                $flippedExtNums[$extension["ExtNo"]] = $i;
            }
            $i++;
        }
        fclose($handle);

        //delete table first
        if ($addExt == false)
        {
            User::truncate();
        }
        else
        {//check for uniqueness within current table
            User::whereIn("ExtNo", $extNums)->delete();
        }

        Eloquent::unguard();
        $chunks = array_chunk($extensions, 100);
        foreach ($chunks as $extsChunks)
        {
            User::insert($extsChunks);
        }

        Helper::refreshUsers();
        return Redirect::to('/assign/extensions');
    }

    //eloquent colleciton passed in, returns the first primary key, 0 if none
    private function findPrim(Illuminate\Database\Eloquent\Collection $collection, $field, $value)
    {//linear search should be fine, assumes relatively few divs/ sites, < 1000
        foreach($collection as $item)
        {
            if ($item->$field == $value)
                return $item->getKey();
        }

        return null;
    }

    private function json_to_user($jsonUser, $user)
    {//there has to be a better way
        try
        {
            //$user = new User;
            $user->UserID   =isset($jsonUser->UserID  ) ?  $jsonUser->UserID  :null;
            $user->Ext      =isset($jsonUser->Ext     ) ?  $jsonUser->Ext     :null;
            $user->Name     =isset($jsonUser->Name    ) ?  $jsonUser->Name    :null;
            $user->Email    =isset($jsonUser->Email   ) ?  $jsonUser->Email   :null;
            $user->AuthCode =isset($jsonUser->AuthCode) ?  $jsonUser->AuthCode:null;
            $user->Notes    =isset($jsonUser->Notes   ) ?  $jsonUser->Notes   :null;
            $user->LoginA   =isset($jsonUser->LoginA  ) ?  $jsonUser->LoginA  :null;
            $user->LoginB   =isset($jsonUser->LoginB  ) ?  $jsonUser->LoginB  :null;
            $user->SiteNo   =isset($jsonUser->SiteNo  ) ?  $jsonUser->SiteNo  :null;
            $user->DivANo   =isset($jsonUser->DivANo  ) ?  $jsonUser->DivANo  :null;
            $user->DivBNo   =isset($jsonUser->DivBNo  ) ?  $jsonUser->DivBNo  :null;
            $user->DivCNo   =isset($jsonUser->DivCNo  ) ?  $jsonUser->DivCNo  :null;
            $user->DivDNo   =isset($jsonUser->DivDNo  ) ?  $jsonUser->DivDNo  :null;
            $user->DivENo   =isset($jsonUser->DivENo  ) ?  $jsonUser->DivENo  :null;
            $user->RateNo   =isset($jsonUser->RateNo  ) ?  $jsonUser->RateNo  :null;
            $user->Color    =isset($jsonUser->Color   ) ?  $jsonUser->Color   :null;
            $user->Print    =isset($jsonUser->Print   ) ?  $jsonUser->Print   :null;
            $user->Device   =isset($jsonUser->Device  ) ?  $jsonUser->Device  :null;
            $user->Private  =isset($jsonUser->Private ) ?  $jsonUser->Private :null;
            $user->PMSPost  =isset($jsonUser->PMSPost ) ?  $jsonUser->PMSPost :null;
            $user->Auto     =isset($jsonUser->Auto    ) ?  $jsonUser->Auto    :null;
            $user->UserIDNo =isset($jsonUser->UserID) ?  intval($jsonUser->UserID): null;
            $user->ExtNo    =isset($jsonUser->Ext   ) ?  intval($jsonUser->Ext)   : null;
            $user->SENo     =isset($jsonUser->SENo    ) ?  $jsonUser->SENo    :null;

        }catch (Exception $e) {
            throw($e);
        }

        return $user;
    }

    public function getExtensionTable()
    {
         return Response::view('includes/extension_table')->header('Content-Type', 'text/html');
    }
}

?>
