@extends('extensions_layout')

<?php
    $searchFilter = Input::get('filter', '');
    $searchValue = Input::get('value', '');
    $sortBy = Input::get('sortBy', '');
    $sortOrder =  Input::get('sortOrder', 'ASC');
    $filterType = "";
    $orderName = "";
    switch($searchFilter)
    {
        case 0: $filterType = "UserID";break;
        case 1: $filterType = "ExtNo";break;
		case 2: $filterType = "Name";break;
        case 3: $filterType = "RateNo";break;
		case 4: $filterType = "DivANo";break;
		case 5: $filterType = "DivBNo";break;
		case 6: $filterType = "DivCNo";break;
		case 7: $filterType = "DivDNo";break;
		case 8: $filterType = "DivENo";break;

    }
	switch($sortBy)
    {
        case 0: $orderName = "UserIDNo";break;
        case 1: $orderName = "ExtNo";break;
		case 2: $orderName = "Name";break;
        case 3: $orderName = "RateNo";break;
		case 4: $orderName = "DivANo";break;
		case 5: $orderName = "DivBNo";break;
		case 6: $orderName = "DivCNo";break;
		case 7: $orderName = "DivDNo";break;
		case 8: $orderName = "DivENo";break;
		case 9: $orderName = "SiteNo";break;
		case 10: $orderName = "Notes";break;
    }
    if($searchValue == '' )
    {
		if($sortOrder == '')
		{
        	$users = User::orderBy('UserIDNo', 'asc')->paginate(200);
        	$user_links = $users->links();
		}
		else
		{
			$users = User::orderBy($orderName, $sortOrder)->paginate(200);
        	$user_links = $users->appends(
				array('sortBy' => $sortBy,
					  'sortOrder' => $sortOrder)
				)->links();
		}

    }
    else
    {
		if($sortOrder == '')
		{
			$users = User::where($filterType, 'LIKE', '%'.$searchValue.'%')
							->orderBy('UserIDNo', 'asc')->paginate(200);

			$user_links = $users->appends(
				array('filter' => $filterType,
					  'value' => $searchValue
					  )
				)->links();
		}
		else
		{
			$users = User::where($filterType, 'LIKE', '%'.$searchValue.'%')
							->orderBy($orderName, $sortOrder)->paginate(200);

			$user_links = $users->appends(
				array('filter' => $filterType,
					  'value' => $searchValue,
					  'sortBy' => $sortBy,
					  'sortOrder' => $sortOrder)
				)->links();
		}
    }

    $types = array(
        'A',
        'B',
        'C',
        'D',
        'E',
    );

    //rates
    $rates = Ini00::where('Name','like','RateTitle%')->
                    orWhere('Name','MaxRateTiers')->get();
    $maxRates = $rates->find('MaxRateTiers')->Value;
    $rates->forget(0); // assumes MaxRateTiers is always first....

    //Divs
    $ini0Info = Ini0::where('Name','like','DivisionLevel%')->
                    orWhere('Name', 'MultisiteEnable')->get();

    foreach($types as $t)
    {
        $DivModel  = "Div".$t;
        $divsEnabled = "divs".$t."Enabled";
        $divsName = "divs".$t;
        ${$divsEnabled} = false;
        if ($ini0Info->find("DivisionLevel".$t."Enabled")->Value){
            ${$divsName} = $DivModel::all();
            ${$divsEnabled} = true;
        }
    }

    //Sites
    $multisiteEnable = $ini0Info->find('MultisiteEnable')->Value;
    $sites = Site::all();
?>


<?php //Divs comboBoxes
    foreach ($types as $t)
    {
        $divsEnabled = "divs".$t."Enabled";
        echo Form::hidden($divsEnabled, ${$divsEnabled} ? "1" : "0");
        if (${$divsEnabled})
        {
            $divsName = "divs".$t;
            $divs = ${$divsName};
            $namesArray = array();
            foreach($divs as $div)
            {
                $namesArray[$div->{"Div".$t."No"}] = $div->{"Div".$t."Name"};
            }
            ?>
            @section("extensions_divs".$t."_comboBox")
                {{ Form::select($divsName, $namesArray, 'default', array('id'=>$divsName, 'style'=>'width:205px')) }}
            @stop
            <?php
        }
    }
?>

@section('extensions_rates_comboBox')
<?php

    $namesArray = array();
    $i = 0;
    foreach($rates as $rate)
    {
        if ($i >= $maxRates)
            break;
        $namesArray[intval(substr($rate->Name, -2)) ] = $rate->Value;
        $i++;
    }
    echo Form::select("ratesCbx", $namesArray,  'default', array('id'=>'ratesCbx', 'style' => 'width:205px'));
?>
@stop

@section('extensions_sites_comboBox')
<?php
    if ($multisiteEnable)
    {
        $namesArray = array();
        $i = 0;
        foreach($sites as $site)
        {
            $namesArray[$site->SiteNo] = $site->SiteLongName;
            $i++;
        }

        echo Form::select("sitesCbx", $namesArray, array('id'=>'sitesCbx'), array('style' => 'width:205px'));
    }
?>
@stop
@section('extensions_color_picker')
<?php

    $valuesArray = array("Black", "Maroon", "Green", "Olive", "Navy", "Purple", "Teal", "Gray", "Silver", "Red", "Lime", "Yellow", "Blue", "Fuchsia", "Aqua", "White");
    $i = 0;
?>
 <select name="colorSelect" id="colorSelect">
         @foreach ($valuesArray as $values)
           <option value={{$i}} style="background:{{$values}}; background-repeat:no-repeat;width:180px" >{{$values}}</option>
         <?php $i++ ?>
         @endforeach
</select>
@stop
@section('extensions_content')

<div id="contentExtTable">
    <div id="testtable" class="extTableDiv" >
<table id="user_datagrid_tab0" class="calls_datagrid_tabExt " style="border: 1px solid black;" >
<thead>
    <th id="ldapThTd" >#</th>
    <th id="ldapThTd">User</th>
    <th id="ldapThTd" style="display:none">UserIDNo</th>
    <th id="ldapThTd">Ext</th>
    <th id="ldapThTd">Name</th>
    <th id="ldapThTd">Rate</th>
    <th id="ldapThTd" style="display:none">RateNo</th>
    @foreach($types as $t)
        @if (${"divs".$t."Enabled"})
            <th id="ldapThTd">{{ $ini0Info->find("DivisionLevel".$t."DescS")->Value }}</th>
            <th id="ldapThTd" style="display:none">{{ "Div".$t."No" }}</th>
        @else
            <th id="ldapThTd" style="display:none"></th>
            <th id="ldapThTd" style="display:none"></th>
        @endif
    @endforeach
    @if($multisiteEnable)
        <th id="ldapThTd">Site</th>
        <th id="ldapThTd" style="display:none">SiteNo</th>
    @else
        <th id="ldapThTd" style="display:none"></th>
        <th id="ldapThTd" style="display:none"></th>
    @endif
    <th id="ldapThTd" style="display:none">Color</th>
    <th id="ldapThTd" style="display:none">ColorNo</th>
    <th id="ldapThTd" style="display:show">Notes</th>
    <th id="ldapThTd">Edit</th>
    <th id="ldapThTd">Delete</th>
    <th id="ldapThTd" style="display:none">Email</th>
    <th id="ldapThTd" style="display:none">Auth Code</th>


</thead>
<tbody>
   <?php $i = 1?>
    <?php  $valuesArray = array("Black", "Maroon", "Green", "Olive", "Navy", "Purple", "Teal", "Gray", "Silver", "Red", "Lime", "Yellow",
                                "Blue", "Fuchsia", "Aqua", "White"); ?>
@foreach ($users as $user)
<tr id="ldapThTd">
    <td id="ldapThTd">{{ $i }}</td>
    <td id="ldapThTd">{{ $user->UserID }}</td>
    <td id="ldapThTd" style="display:none">{{ $user->UserIDNo }}</td>
    <td id="ldapThTd">{{ $user->Ext }}</td>
    <td id="ldapThTd">{{ $user->Name }}</td>
    <td id="ldapThTd">{{ $rates->find("RateTitle".sprintf("%02s", $user->RateNo))->Value }}</td>
    <td id="ldapThTd" style="display:none">{{ $user->RateNo }}</td>
    @foreach($types as $t)
        @if (${"divs".$t."Enabled"})
            <td id="ldapThTd">
            <?php
                $userDiv = ${"divs".$t}->find($user->{"Div".$t."No"});
                if (isset($userDiv))
                    echo $userDiv->{"Div".$t."Name"};
                else
                    echo 'N/A';
            ?>
            </td>
            <td id="ldapThTd" style="display:none">{{ $user->{"Div".$t."No"} }}</td>
        @else
            <td id="ldapThTd" style="display:none"></td>
            <td id="ldapThTd" style="display:none"></td>
        @endif
    @endforeach
    @if($multisiteEnable)
        <td id="ldapThTd">{{ $sites->find($user->SiteNo)->SiteLongName }}</td>
        <td id="ldapThTd" style="display:none">{{ $user->SiteNo }}</td>
    @else
        <td id="ldapThTd" style="display:none"></td>
        <td id="ldapThTd" style="display:none"></td>
    @endif
    <td id="ldapThTd" style="color: {{ $valuesArray[$user->Color] }}; display:none">{{ $valuesArray[$user->Color] }}</td>
    <td id="ldapThTd" style="display:none">{{ $user->Color }}</td>
    <td id="ldapThTd" style="display:show">{{ $user->Notes }}</td>
    <td id="ldapThTd"><a style="text-decoration:underline;" id="editbtn" href='#' onclick='SetEditValues({{ $i }})'>Edit</a></td>
    <td id="ldapThTd">
        <form id="deleteCbx" action="">
            <input type="checkbox" name="{{ "delete".$i }}" id="{{ "delete".$i }}">
        </form>
    </td>
    <td id="ldapThTd" style="display:none">{{ $user->Email }}</td>
    <td id="ldapThTd" style="display:none">{{ $user->AuthCode }}</td>

</tr>
<?php $i++ ?>
@endforeach
</tbody>
</table>
</div>
<div style="margin-top:15px">
     {{ $user_links }}
</div>
</div>


@stop


@section('searching_cbbx')
<select id="filter" name="filter" onchange="FilterSearchBySelection()">
	<option value="0">UserID</option>
	<option value="1">Ext</option>
	<option value="2">Name</option>
	<option value="3">Rate</option>
	@if($divsAEnabled == 1)
	<option value="4">{{ $ini0Info->find("DivisionLevelADescS")->Value }}</option>
	@endif
	@if($divsBEnabled == 1)
	<option value="5">{{ $ini0Info->find("DivisionLevelBDescS")->Value }}</option>
	@endif
	@if($divsCEnabled == 1)
	<option value="6">{{ $ini0Info->find("DivisionLevelCDescS")->Value }}</option>
	@endif
	@if($divsDEnabled == 1)
	<option value="7">{{ $ini0Info->find("DivisionLevelDDescS")->Value }}</option>
	@endif
	@if($divsEEnabled == 1)
	<option value="8">{{ $ini0Info->find("DivisionLevelEDescS")->Value }}</option>
	@endif
</select>
@stop

@section('sorting_cbbx')
<select id="sortingCbbx" name="sortingCbbx" >
	<option value="0" >User</option>
	<option value="1">Ext</option>
	<option value="2" >Name</option>
	<option value="3" >Rate</option>
	@if($divsAEnabled == 1)
	<option value="4" >{{ $ini0Info->find("DivisionLevelADescS")->Value }}</option>
	@endif
	@if($divsBEnabled == 1)
	<option value="5" >{{ $ini0Info->find("DivisionLevelBDescS")->Value }}</option>
	@endif
	@if($divsCEnabled == 1)
	<option value="6" >{{ $ini0Info->find("DivisionLevelCDescS")->Value }}</option>
	@endif
	@if($divsDEnabled == 1)
	<option value="7" >{{ $ini0Info->find("DivisionLevelDDescS")->Value }}</option>
	@endif
	@if($divsEEnabled == 1)
	<option value="8" >{{ $ini0Info->find("DivisionLevelEDescS")->Value }}</option>
	@endif
	<option value="9" >Site</option>
	<option value="10" >Notes</option>
</select>
@stop

@section('rates_cbbx_search')
<?php

    $namesArray = array();
    $i = 0;
    foreach($rates as $rate)
    {
        if ($i >= $maxRates)
            break;
        $namesArray[intval(substr($rate->Name, -2)) ] = $rate->Value;
        $i++;
    }
    echo Form::select("ratesCbxSearch", $namesArray,  'default', array('id'=>'ratesCbxSearch', 'style' => 'display:none'));
?>
@stop

<?php //Divs comboBoxes
    foreach ($types as $t)
    {
        $divsEnabled = "divs".$t."Enabled";
        echo Form::hidden($divsEnabled, ${$divsEnabled} ? "1" : "0");
        if (${$divsEnabled})
        {
            $divsName = "divs".$t;
			$divsSearchingName = "divs".$t."Searching";
            $divs = ${$divsName};
            $namesArray = array();
            foreach($divs as $div)
            {
                $namesArray[$div->{"Div".$t."No"}] = $div->{"Div".$t."Name"};
            }

            ?>
            @section("extensions_divs_search".$t."_comboBox")
                {{ Form::select($divsSearchingName, $namesArray, 'default', array('id'=>$divsSearchingName, 'style'=>'display:none')) }}
            @stop
            <?php
        }
    }
?>

