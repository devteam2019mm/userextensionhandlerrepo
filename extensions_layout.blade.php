@extends('main_layout')

@section('main_menu')
<script src="/js/extension.js"></script>
<link href="/css/pagination.css" rel="stylesheet">
    <ul id="jMenu" style="width:40px">
        <li ><a href="#" onmouseover="">Advanced</a>
        <ul>
            <li id="importExt"><a href="#" onclick="showFileDialog()">Import Extensions</a>	</li>
            <li id="exportExt"><a href="/assign/extensions/export">Export Extensions</a>	</li>
            <li id="deleteallUnk"><a href="#" onclick="DeleteAllUnkM(1)">Delete All "Unknown" Extensions</a>	</li>
        </ul>
        </li>
    </ul>
@stop

@section('main_page_title')
    Extensions
@stop

@section('main_back_button')
  <a href="/"><img src="/img/back-icon1.png" onmouseover=" this.src='/img/back-iconAct.png'" onmouseout=" this.src='/img/back-icon1.png'" alt="" width="40" height="40"></a>
@stop

@section('main_content')

 <div id="bigSpace" class="boxExtension">
 <div id="space1" style="height:10px">
 </div>
  <div style="margin-left:10px; margin-top:5px; ">
  <table>
  	 <tr>
  	 	<td>
			<fieldset style="height:60px; width:350px">
			<legend>Find Extension</legend>
			<form class = "form" name="searchForm" id="searchForm" method="post" action="{{ url('/assign/extensions') }}" >
			<table>
			<tr>
			<td>
			 By
			</td>
			<td>
			@yield("searching_cbbx")
			</td>
			<td>
			 <input type="text" id="searchValue" name="searchValue" size="12">
			</td>
			<td>
			@yield('rates_cbbx_search')
			</td>
			<td> @yield('extensions_divs_searchA_comboBox')</td>
			<td> @yield('extensions_divs_searchB_comboBox')</td>
			<td> @yield('extensions_divs_searchC_comboBox')</td>
			<td> @yield('extensions_divs_searchD_comboBox')</td>
			<td> @yield('extensions_divs_searchE_comboBox')</td>
			<td>
			 <input type="button" id="search_btn" value="Search">
			</td>
			</tr>
			</table>
			</form>
			</fieldset>
  	 	</td>
  	 	<td>
			<fieldset style="height:60px; width:405px">
			<legend>Sort </legend>
			<table >
			<tr>
			<td>Sort By:</td>
			<td>
				@yield('sorting_cbbx')
			</td>
			<td>Order:</td>
			<td>
				<select id="sortType" name="sortType">
					<option value="0">ASC</option>
					<option value="1">DESC</option>
				</select>
			</td>
			<td><div style="width:20px"></div></td>
			<td><input type="button" id="sort_btn" value="Sort"></td>
			</tr>
			</table>
			</fieldset>
  	 	</td>
  	 </tr>
  </table>
 </div>
 <div id="divAddBtn" style="margin-left:10px; margin-top:10px; ">
     <table>
		 <tr>
			 <td>
				 <input name="add-ext" id="add-ext" type="button" value="Add" >
			 </td>
			 <td>
				 <input name="del-ext" id="del-ext" type="button" value="Del" onclick="DeleteUsers()" >
			 </td>
		 </tr>
     </table>
 </div>
 <div style="margin-top:10px"></div>
 @yield('extensions_content')
 <div id="displayDialog" >
    <div id="dialog-form" title="Add/Edit"  >
      <form action="" method="get">
        <div id="userTD">
            <label id="userId">User Id</label>
        </div>
        <div id="userTxtD" style="margin-left:110px; margin-top:-20px">
            <input name="userIdTxt"  id="userIdTxt" type="text" size="20" />
        </div>
         <div id="extTD" style="margin-top:10px">
            <label id="ext">Extension</label>
        </div>
        <div id="extTxtD" style="margin-left:110px; margin-top:-20px">
            <input name="extTxt" id="extTxt" type="text" size="20" />
        </div>
        <div id="nameTD" style="margin-top:10px">
            <label id="ext">Name</label>
        </div>
        <div id="nameTxtD" style="margin-left:110px; margin-top:-20px">
            <input name="nameTxt" id="nameTxt" type="text" size="20" />
        </div>
        <div id="rateTD" style="margin-top:10px">
            <label id="rate">Rate</label>
        </div>
        <div id="rateTxtD" style="margin-left:110px; margin-top:-35px">
            @yield('extensions_rates_comboBox')
        </div>
        <div id="divATD" style="margin-top:10px; ">
            <label id="divATxt"></label>
        </div>
        <div id="divATxtD" style="margin-left:110px; margin-top:-20px">
            @yield('extensions_divsA_comboBox')
        </div>
        <div id="divBTD" style="margin-top:15px; ">
            <label id="divBTxt"></label>
        </div>
        <div id="divBTxtD" style="margin-left:110px; margin-top:-20px">
            @yield('extensions_divsB_comboBox')
        </div>
        <div id="divCTD" style="margin-top:15px; ">
            <label id="divCTxt"></label>
        </div>
        <div id="divCTxtD" style="margin-left:110px; margin-top:-20px">
            @yield('extensions_divsC_comboBox')
        </div>
        <div id="divDTD" style="margin-top:15px; ">
            <label id="divDTxt"></label>
        </div>
        <div id="divDTxtD" style="margin-left:110px; margin-top:-20px">
            @yield('extensions_divsD_comboBox')
        </div>
        <div id="divETD" style="margin-top:15px; ">
            <label id="divETxt"></label>
        </div>
        <div id="divETxtD" style="margin-left:110px; margin-top:-20px">
            @yield('extensions_divsE_comboBox')
        </div>
        <div id="SiteTD" style="margin-top:15px; display:none">
            <label id="Site">Site</label>
        </div>
       <div id="SiteTxtD" style="margin-left:110px; margin-top:-35px">
            @yield('extensions_sites_comboBox')
        </div>
        <div id="emailDiv" style="margin-top:15px">
            <label id="Email">Email</label>
        </div>
        <div id="emailTxtD" style="margin-left:110px; margin-top:-20px">
            <input name="emailTxt" id="emailTxt" type="text" size="20" />
        </div>
         <div id="authCodeDiv" style="margin-top:10px">
            <label id="authCode">Auth Code</label>
        </div>
        <div id="authCodeTxtD" style="margin-left:110px; margin-top:-20px">
            <input name="authCodeTxt" id="authCodeTxt" type="text" size="20" />
        </div>
         <div id="NotesDiv" style="margin-top:10px">
            <label id="notes">Notes</label>
        </div>
        <div id="notesTxtD" style="margin-left:110px; margin-top:-20px">
            <input name="notesTxt" id="notesTxt" type="text" size="20" />
        </div>
        <div id="message" style="margin-top:15px; display:none">
            <label id="lblMessg">text</label>
        </div>

        <div id="colorPickerL" style="margin-top:10px;">
           <label>Color:</label>
        </div>
        <div id="colorPickerD" style="margin-left:110px; margin-top:-30px">
            @yield('extensions_color_picker')
        </div>

      </form>
    </div>
    <div id="dialog-confirm" title="Delete Extensions">
     <p>You are about to delete all users and extensions.Do you want to continue?</p>
   </div>
   <div id="dialog-confirm1" title="Delete Extensions">
     <p> Extensions will be deleted.Do you want to continue?</p>
   </div>

   <div id="dialog-confirmImport" title="Warning">
     <p> You are about to overwrite all users and extensions.Do you want to continue?</p>
   </div>

   <div id="dialog-file" title="Import">
        <form id ="ImportForm" action='/assign/extensions/import' onsubmit="" method="POST" enctype="multipart/form-data" files=true >
        <input type="file" name="file" id="file">
        <div id=""testingHeader style="margin-top:10px; margin-left:0px">
            <input type="text" name="testingH" id="testingH" value="" size="42">
        </div>
        <div id="columns" style="margin-top:20px">
            <div id="columnLeft" style="height:200px">
            <label id="userIdLbl">User ID:</label>
            <div id="UserIDDiv" class="styled-select">
             <select name="UserID" id="UserID" >
            @for($i = 0; $i < 16; $i++)
                    <option value="{{$i}}">{{$i}}</option>
                 @endfor
             </select>
           </div>
           <div id="extD" style="margin-top:20px;">
               <label id="extLbl">Extension:</label>
           </div>
           <div id="ExtDiv" class="styled-select">
             <select name="Ext" id="Ext">
                @for($i = 0; $i < 16; $i++)
                    <option value="{{$i}}">{{$i}}</option>
                 @endfor
             </select>
           </div>
            <div id="nameD" style="margin-top:20px;">
               <label id="nameLbl">Name:</label>
           </div>
           <div id ="nameDiv" class="styled-select">
             <select name="Name" id="Name">
              @for($i = 0; $i < 16; $i++)
                    <option value="{{$i}}">{{$i}}</option>
                 @endfor
             </select>
           </div>
           <div id="DivANoD" style="margin-top:20px;">
               <label id="DivANoLbl">Department:</label>
           </div>
           <div id="DivANoDiv"class="styled-select">
             <select name="DivANo" id="DivANo">
                @for($i = 0; $i < 16; $i++)
                    <option value="{{$i}}">{{$i}}</option>
                 @endfor
             </select>
           </div>
           <div id="DivBNoD" style="margin-top:20px;">
               <label id="DivBNoLbl">Division:</label>
           </div>
           <div id="DivBNoDiv"class="styled-select">
             <select name="DivBNo" id="DivBNo" >
                 @for($i = 0; $i < 16; $i++)
                    <option value="{{$i}}">{{$i}}</option>
                 @endfor
             </select>

           </div>
           <div id="DivCNoD" style="margin-top:20px;">
               <label id="DivCNoLbl">Branch:</label>
           </div>
           <div id="DivCNoDiv"class="styled-select">
             <select name="DivCNo" id="DivCNo"  >
                 @for($i = 0; $i < 16; $i++)
                    <option value="{{$i}}">{{$i}}</option>
                 @endfor
             </select>
           </div>
           <div id="DivDNoD" style="margin-top:20px;">
               <label id="DivDNoLbl">Organization:</label>
           </div>
           <div id="DivDNoDiv"class="styled-select">
             <select name="DivDNo" id="DivDNo" >
                 @for($i = 0; $i < 16; $i++)
                    <option value="{{$i}}">{{$i}}</option>
                 @endfor
             </select>
           </div>
          <div id="DivENoD" style="margin-top:20px; display:none">
               <label id="DivENoLbl"></label>
           </div>
           <div id="DivENoDiv" class="styled-select" style="display:none">
             <select name="DivENo" id="DivENo">
                 @for($i = 0; $i < 16; $i++)
                    <option value="{{$i}}">{{$i}}</option>
                 @endfor
             </select>
           </div>
           </div>

          <div id="notesD" style="margin-top:-200px; margin-left:200px">
               <label id="siteLbl">Notes:</label>
           </div>
           <div id="notesDiv" class="styled1-select" >
             <select name="Notes" id="Notes" >
                 @for($i = 0; $i < 15; $i++)
                    <option value="{{$i}}">{{$i}}</option>
                 @endfor
             </select>
           </div>
           <div id="authD" style="margin-top:20px; margin-left:200px">
               <label id="authLbl">Auth Code:</label>
           </div>
           <div id="authDiv" class="styled1-select" >
             <select name="AuthCode" id="AuthCode">
                 @for($i = 0; $i < 15; $i++)
                    <option value="{{$i}}">{{$i}}</option>
                 @endfor
             </select>
           </div>
          <div id="rateD" style="margin-top:20px; margin-left:200px">
               <label id="rateLbl">Rate No:</label>
           </div>
           <div id="rateDiv" class="styled1-select" >
             <select name="RateNo" id="RateNo">
                 @for($i = 0; $i < 15; $i++)
                    <option value="{{$i}}" >{{$i}}</option>
                 @endfor
             </select>
           </div>
           <div id="colorTextD" style="margin-top:20px; margin-left:200px">
               <label id="colorLbl">Color:</label>
           </div>
           <div id="colorDiv"  class="styled1-select">
                <select name="Color" id="Color">
                 @for($i = 0; $i < 15; $i++)
                    <option value="{{$i}}" >{{$i}}</option>
                 @endfor
                </select>
           </div>
           <div id="siteD" style="margin-top:20px; margin-left:200px">
               <label id="siteLbl">Site Name:</label>
           </div>
           <div id="siteDiv" class="styled1-select" >
             <select name="SiteNo" id="SiteNo" >
                 @for($i = 0; $i < 15; $i++)
                    <option value="{{$i}}">{{$i}}</option>
                 @endfor
             </select>
           </div>
           <div id="chkSkip" style="margin-top:160px">
               <input type="hidden" name="skip" value="0">
               <input type="checkbox" name="skip" id="skip" value="1">Skip first line in file.
           </div>
           </div>
            <div id="chkDiv" style="margin-top:15px">
                <input type="hidden" name="addExt" value="0">
               <input type="checkbox" name="addExt" id="addExt" value="1">Add to existing extensions.
           </div>

       </form>
   </div>
  <div class="modal"><!-- Place at bottom of page --></div>
 </div>
</div>
@stop
