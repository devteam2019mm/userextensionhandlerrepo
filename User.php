<?php

class User extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users0000';
    
    public $timestamps = false;
    
    //cause
    public $incrementing = false;
    
    public $primaryKey = 'UserIDNo';

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

    public function DivA()
    {
        return $this->belongsTo('DivA','DivANo','DivANo');
    }

    public function DivB()
    {
        return $this->belongsTo('DivB','DivBNo','DivBNo');
    }

    public function DivC()
    {
        return $this->belongsTo('DivC','DivCNo','DivCNo');
    }

    public function DivD()
    {
        return $this->belongsTo('DivD','DivDNo','DivDNo');
    }

    public function DivE()
    {
        return $this->belongsTo('DivE','DivENo','DivENo');
    }
}
