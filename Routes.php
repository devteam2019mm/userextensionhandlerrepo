<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
//requires admin privileges 
    Route::group(array('before' => 'auth.admin'), function()
    {
		Route::any('assign/extensions', function()
        {
            return View::make('extensions');
        });

        Route::any('insert-user', array('before' => 'contains_json',
                                        'uses' => 'ExtensionController@insertUser'));

        Route::any('delete-users', array('uses' => 'ExtensionController@deleteUsers'));

        Route::any('update-user', array('before' => 'contains_json',
                                        'uses' => 'ExtensionController@updateUser'));

        Route::any('assign/extensions/export/{type?}', 'ExtensionController@export');
        Route::any('assign/extensions/import', 'ExtensionController@import');
        Route::get('extension-table', array('uses'=>'ExtensionController@getExtensionTable'));
	}
?>